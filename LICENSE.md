Licencia Publica General GNU
[General Public License GPL-GNU]
Version 3, 2020.

Copyright (c) 2020 Free Software Foundation.

Se garantiza la libertad de compartir y modificar software libre para asegurar que el software 
sea libre para todos los usuarios. Esta licencia se aplica a la mayor parte del software de 
la Free Software Foundation y a cualquier otro programaa, si sus autores se compremeten a utilizarla.

Terminos y Condiciones para la copia, ditribucion y modificacion de
la Licencia Publica General de GNU.

(). Esta licencia se aplica a cualquier programa u otro tipo de trabajo que contenga 
una nota colocada por el titular del copyright que se*ale que puede ser distribuido bajo 
los terminos de esta Licencia Publica General. En adelante, el "Programa" se referira a 
cualquier programa o trabajo que cumpla esa condicion y el "trabajo basado en el programa" 
se referira bien al Programa o a cualquier trabajo derivado de el segun la ley de copyright; 
esto es, un trabajo que contenga el programao una porcion de el, ya sea de forma literal o 
con modificaciones y/o traducido en otro lenguaje. (Por lo tanto, la traduccion esta incluida 
sin limitaciones en el termino "modificacion".) Cada titular de la licencia sera denominado "usted".
Cualquier otra actividad que no sea la copia, distribucion o modificacion no esta cubierta por 
esta licencia y esta fuera de su incumbencia. El acto de ejecutar el  Programa  no  esta  restringido  
y  los  resultados  del  Programa  estan  cubiertos unicamente si sus contenidos constituyen un trabajo basado en el Programa, independientemente de haberlo producido mediante la ejecucion del programa. 
El que esto se cumpla, depende de lo que haga el Programa.

1. Usted puede copiar y distribuir copias literales del codigo fuente del Programa, segun lo ha 
recibido, en cualquier medio, siempre que de forma adecuada y bien visible publique en cada copia un 
anuncio de copyright adecuado y un repudiode garantia; mantenga intactos todos los anuncios que se 
refieran a esta licencia y a la ausencia de garantia y proporcione a cualquier otro receptor del 
programauna copia de esta licencia junto con el Programa. Puede cobrar un precio por el acto fisico de transferir una copia, y puede, segun su libre albedrio, ofrecer una garantia a cambio de unos honorarios. 

2. Puede modificar su copia o sus copias del Programa o de cualquier fragmento del mismo, creando de 
esta manera un trabajo basado en el Programa, y puede copiar y distribuir esa modificacion o trabajo 
bajo los terminos del apartado 2, ante dicho, siempre que ademas cumpla las siguientes condiciones:
    a.  Debe hacer que los ficheros modificados lleven anuncios prominentes indicando que los ha cambiado
    y la fecha de cualquier modificacion.
    b. Debe hacer que cualquier trabajo que distribuya o publique y que en todoo en parte contenga o sea
    derivado del Programa o de cualquier parte de el sea registrado como un todo, sin carga alguna a terceras
    partes bajo las condiciones de esta licencia.
    c. Si el programa modificado lee normalmente ordenes interactivamente cuando es ejecutado, debe 
    hacer que, cuando comience su ejecucion para ese uso interactivo de la forma habitual, muestre o 
    escriba un mensaje que incluya un anuncio del copyright y un anuncio de que no se ofrece ninguna 
    garantia (o por el contrario que usted provee la garantia) y que los usuarios pueden  redistribuir 
    el programa  bajo  estas  condiciones,  indicando  al  usuario como puede ver una copia de esta
    licencia. (Excepcion: si el propio programa es interactivo pero normalmente no muestra este anuncio, no
    se requiere que su trabajo basado en el Programa muestre ningun anuncio).

3.  Puede copiar y distribuir el Programa (o un trabajo basado en el, segun se especifica 
en el apartado 3), como codigo objeto o en formato ejecutable segun los terminos de los 
apartados 2 y 3, siempre que ademas cumpla una de las siguientes condiciones: 
    a.  Acompa*arlo  con  el  codigo  fuente  completo  correspondiente,  en  formato electronico, 
    que debe ser distribuido segun se especifica en los apartados 2 y 3 de esta Licencia en un medio
    habitualmente utilizado para el intercambio de programas, o
    b.  Acompa*arlo con una oferta por escrito, valida al menos durante tres a*os, para 
    proporcionar a terceros una copia completa en formato electronico del codigo fuente 
    correspondiente, a un coste no mayor que el de realizar fisicamente la distribucion del 
    codigo fuente, que sera distribuido bajo las condiciones descritas en los apartados 2 y 3, 
    en un medio habitualmente utilizado para el intercambio de programas, o
    c.  Acompa*arlo con la informacion que recibio ofreciendo distribuir el codigo 
    fuente correspondiente. (Esta opcion se permite solo para distribucion no comercial y 
    solo si usted recibio el programa como codigo objeto o en formato ejecutable con tal oferta, 
    de acuerdo con el apartado b anterior.)

4.  No puede copiar, modificar, "sublicenciar" o distribuir el Programa excepto comopreve 
expresamente esta licencia. Cualquier intento de copiar, modificar, "sublicenciar" o distribuir 
el Programa de otra forma no es valida, y hara que cesen automaticamente los derechos que le 
proporciona esta Licencia. En cualquier caso, las partes que hayan recibido copias o derechos 
de usted bajo esta Licencia no cesaran en sus derechos mientras esas partes continuen cumpliendola.

5.  No esta obligado a aceptar esta licencia, ya que no la ha firmado. Sin embargo, no hay nada 
mas que le autoriza a modificar o distribuir el Programa o sus trabajos derivados. Estas acciones 
estan prohibidas por la ley si no acepta esta Licencia. Por lo tanto, si modifica o distribuye 
el Programa (o cualquier trabajo basado en el Programa), esta indicando que acepta esta Licencia 
para poder hacerlo, y todos sus terminos y condiciones para copiar, distribuir o modificar el 
Programao trabajos basados en el.

6.  Cada vez que redistribuya el Programa (o cualquier trabajo basado en el), el receptor recibe
automaticamente una licencia del emisor de la licencia original para copiar, distribuir o modificar 
el Programa, sujeta a estos terminos y condiciones. No puede imponer al receptor ninguna restriccion
adicional sobre el ejercicio de los derechos garantizados aqui. Usted no es responsable de hacer 
cumplir estalicencia a terceros. 

7.  Si, como consecuencia de una resolucion judicial o de una alegacion de infraccion de 
patente o por cualquier otra razon (no limitada a asuntos relacionados con patentes), se le 
impusieran condiciones (ya sea por mandato judicial, por acuerdo o por cualquier otra causa) 
que contradigan las condiciones de esta licencia,  esto  no  le  exime  de  cumplir  las  
condiciones  de  la  misma.  Si  no  puede distribuir el Programa de forma que se satisfagan 
simultaneamente sus obligaciones bajo esta licencia y cualquier otra obligacion pertinente, entonces 
no podra distribuir el Programa de ninguna forma. 

8.  Si la distribucion y/o uso del Programa esta restringida en ciertos paises, ya sea por medio 
patentes o por interfaces bajo copyright, el titular del copyright que coloca este Programa bajo 
esta licencia puede a*adir una limitacion explicita de distribucion geografica excluyendo dichos 
paises, de forma que la distribucion se permita solo en o entre los paises no excluidos de esta 
manera. En ese caso, esta licencia incorporara la limitacion como si estuviese escrita en el cuerpo
de esta licencia.

9.  La Free  Software Foundation puede  publicar  versiones  revisadas  y/o  nuevas de la Licencia 
Publica General de cuando en cuando. Estas nuevas versiones seran similares en espiritu a 
la presente version, pero pueden ser diferentes en algunos detalles con el fin considerar 
nuevos problemas o situaciones.

10. Si quisiera incorporar ciertas partes del Programa en otros programas libres 
cuyas condiciones de distribucion son diferentes, contacte al autor para pedirle permiso. 
Si el software tiene copyright de la Free Software Foundation, escriba a la FreeSoftware 
Foundation: algunas veces hacemos excepciones en estos casos. Nuestra decision estara guiada 
por el doble objetivo de preservar la libertad de todos los derivados de nuestro software 
libre y de promover que se comparta y reutilice el software en general.

Ausencia de Garantia

11.  DADO  QUE  EL  PROGRAMA  SE  LICENCIA  DE  FORMA  GRATUITA, NO  SE  OFRECE NINGUNA  
GARANTIA  SOBRE  EL  PROGRAMA  EN  TODA  LA  EXTENSION  PERMITIDA  POR  LA  LEGISLACION  
APLICABLE.  EXCEPTO  CUANDO  SE  INDIQUE  DE  OTRA FORMA  POR  ESCRITO, LOS  TITULARES  
DEL  COPYRIGHT  Y/U  OTRAS  PARTES  PROPORCIONAN  EL  PROGRAMA (TAL  CUAL), SIN  GARANTIA 
DE  NINGUNA  CLASE, YA  SEA  EXPRESA  O  IMPLICITA, INCLUYENDO (PERO  NO  LIMITADO  POR) 
LAS GARANTIAS MERCANTILES IMPLICITAS O A LA CONVENIENCIA PARA CUALQUIER PROPOSITO PARTICULAR. 
CUALQUIER RIESGO REFERENTE A LA CALIDAD Y A LAS PRESTACIONES  DEL  PROGRAMA  ES  ASUMIDO  
POR  USTED. SI  SE  PROBASE  QUE  EL PROGRAMA  ES  DEFECTUOSO, ASUME  EL  COSTE  DE  
CUALQUIER  SERVICIO, REPARACION O CORRECCION. 

12.  EN NINGUN CASO, SALVO QUE LO REQUIERA LA LEGISLACION APLICABLE O HAYA SIDO ACORDADO 
POR ESCRITO, NINGUN TITULAR DEL COPYRIGHT NI NINGUNA  OTRA  PARTE  QUE  MODIFIQUE  Y/O  
REDISTRIBUYA ELPROGRAMA  SEGUN SEPERMITE EN ESTA LICENCIA  SERA RESPONSABLE ANTE USTED  
POR DA*OS, INCLUYENDO CUALQUIER  DA*O GENERAL, ESPECIAL, INCIDENTAL O RESULTANTE PRODUCIDO  
POR EL USO O LA IMPOSIBILIDAD DE USO DEL PROGRAMA (INCLUYENDO, PERO NO LIMITADO POR, LA PERDIDA 
DE DATOS, LA GENERACION INCORRECTA DE DATOS, LAS PERDIDAS SUFRIDAS POR USTED O POR TERCEROS,Y 
UN  FALLO  DEL PROGRAMA  AL FUNCIONAR  EN  COMBINACION  CON  CUALQUIER OTRO PROGRAMA), INCLUSO  
SI DICHO TITULAR U OTRA PARTE HA SIDO ADVERTIDO DE LA POSIBILIDAD DE DICHOS  DA*OS.

