Automatización de Procesos.
===========================

Este espacio fue creado con la intensión de presentar y exponer los procesos que se han logrado automatizar dentro del Proyecto Canaima. En la [Wiki](https://gitlab.com/ProyectoCanaima/automatizacion-de-procesos/-/wikis/home) de este proyecto, podrán encontrar documentación técnica que será de utilidad para la comprensión de cada uno de los procesos que están automatizados e infomación a fin.

Antes de copiar, distribuir o modificar nuestros códigos, recomendamos leer el archivo [LICENSE.md.](https://gitlab.com/ProyectoCanaima/automatizacion-de-procesos/-/blob/master/LICENSE.md)
