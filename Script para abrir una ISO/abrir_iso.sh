#! /bin/sh

# Script para abrir ISO

echo "\n"
echo "\e[1;32m*-----------------------------------------\e[0m" 
echo "\e[1;32m       SCRIPT PARA ABRIR UNA ISO          \e[0m"
echo "\e[1;32m*-----------------------------------------\e[0m \n" 

echo "\e[32mPresione enter para continuar: \e[0m"
read a


echo "\e[93m"


# Se instalan los paquetes necesarios para abrir y cerrar una ISO

echo "Instalando los paquetes necesarios"

sudo apt-get install xorriso
sudo apt install isolinux
sudo apt-get install rsync


# Se crean los directorios y subdiretorios, donde se alojarán los archivos de la ISO

echo "Se crea el directorio modiso y se ingresa en él \n"
mkdir modiso
cd modiso
echo ""

echo "Se crean los subdirectorios loopdir, isofiles y unsquashed \n"
mkdir loopdir isofiles unsquashed
echo "" 

echo "\e[1;32m"
echo "*-----------------------------------------"
echo ""
echo "Escriba la ruta donde se aloja la ISO"
echo "Por ejemplo: /ruta_de_la_ISO"
echo ""
echo "*-----------------------------------------"
echo "\e[0m"

echo "\e[93m"

echo "Directorio actual:"
pwd
echo "\n"
echo "Contenido del directorio:"
ls
echo "\n"

echo "Ruta de la ISO: " 
read RUTA

echo "\e[1;32m"
echo "*-----------------------------------------"
echo ""
echo "Escriba el nombre de la iso, por ejemplo:"
echo "nombre.iso"
echo ""
echo "*-----------------------------------------"

echo "\n"

echo "Nombre de la ISO: "
read ISO

echo "\e[0m"

sudo mount -o loop $RUTA/$ISO loopdir/

rsync -a -H --exclude=TRANS.TBL loopdir/ isofiles/

sudo umount $RUTA/$ISO 

sudo unsquashfs -f -d unsquashed/ isofiles/live/filesystem.squashfs

sudo cp -pv /etc/resolv.conf unsquashed/etc/
sudo cp -pv /etc/hosts unsquashed/etc/
sudo cp -pv /etc/hostname unsquashed/etc/

sudo mount -t proc proc unsquashed/proc/
sudo mount -t sysfs sysfs unsquashed/sys
sudo mount -o bind /dev unsquashed/dev/
sudo mount -t devpts pts unsquashed/dev/pts/

LANG=C sudo chroot unsquashed/ /bin/bash

echo "\e[0m"
