#! /bin/sh

# Script para cerrar ISO


echo "\n"
echo "\e[1;32m*-----------------------------------------\e[0m" 
echo "\e[1;32m      SCRIPT PARA CERRAR UNA ISO          \e[0m"
echo "\e[1;32m*-----------------------------------------\e[0m \n" 

echo "\e[32mPresione enter para continuar: \e[0m"
read a

echo "Instalando los pauqtes necesarios"

sudo apt-get install xorriso
sudo apt install isolinux
sudo apt-get install rsync

sync

echo "Indique la ruta exacta de la carpeta ISO que se creó con anterioridad: \n"
echo "Por ejemplo: /home/usuario"
read RUTA

echo "Ingresando en la carpeta..." 

cd $RUTA/ISO/modiso

sudo umount unsquashed/proc/
sudo umount unsquashed/sys/
sudo umount unsquashed/dev/pts/
sudo umount unsquashed/dev/

sudo mksquashfs unsquashed/ filesystem.squashfs.new

cp filesystem.squashfs.new isofiles/live/filesystem.squashfs

cd isofiles/

sudo xorriso -as mkisofs -R -r -J -joliet-long -l -cache-inodes -isohybrid-mbr /usr/lib/ISOLINUX/isohdpfx.bin -partition_offset 16 -A "Canaima 7 Iwamari" -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o canaima-live-7.0.0-amd64-gnome.iso .

